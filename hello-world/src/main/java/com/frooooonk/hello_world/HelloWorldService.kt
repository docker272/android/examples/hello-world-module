package com.frooooonk.hello_world

class HelloWorldService {
    companion object {
        val prefix = "Hello World "
    }

    fun helloWorld(name: String): String = "$prefix$name"
}