package com.frooooonk.hello_world

import org.junit.Assert.assertEquals
import org.junit.Test


/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class HelloWorldServiceTest {
    @Test
    fun greeting_isCorrect() {
        val name = "foobar"
        assertEquals("${HelloWorldService.prefix}$name", HelloWorldService().helloWorld(name))
    }
}